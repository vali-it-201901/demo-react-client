import React, { Component } from 'react';
import './App.css';
import Companies from './Companies';
import CompanyModal from './CompanyModal';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = { companies: [] };

    this.getCompanies = this.getCompanies.bind(this);
  }

  componentDidMount() {
    this.getCompanies();
  }

  getCompanies() {
    fetch('http://localhost:8080/companies')
      .then(response => response.json())
      .then(companies => this.setState({ companies: companies }));
  }

  render() {
    return (
      <div id="mainContainer">
        <h1>Lahedad ettevõtted</h1>
        <Companies
          companies={this.state.companies}
          onCompanyChange={this.getCompanies}
        />
        <br />
        <br />
        <CompanyModal
          buttonLabel="Lisa ettevõte"
          modalTitle="Ettevõtte lisamine"
          company={{
            id: 0,
            name: null,
            logo: null
          }}
          onCompanyChange={this.getCompanies}
          color="primary"
        />
      </div>
    );
  }
}
