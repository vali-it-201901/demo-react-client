import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Input } from 'reactstrap';
import ValidationError from './ValidationError';

export default class CompanyModal extends Component {
    constructor(props) {
    super(props);
    this.state = {
      modal: false,
      companyName: this.props.company.name,
      companyLogo: this.props.company.logo,
      errorText: null
    };

    this.toggle = this.toggle.bind(this);
    this.handleCompanyNameChange = this.handleCompanyNameChange.bind(this);
    this.handleCompanyLogoChange = this.handleCompanyLogoChange.bind(this);
    this.save = this.save.bind(this);
    this.validate = this.validate.bind(this);
  }

  toggle() {
    if (this.state.modal) {
      this.setState({
        modal: false,
        companyName: null,
        companyLogo: null,
        errorText: null
      });
    } else {
      this.setState({
        modal: true,
        companyName: this.props.company.name,
        companyLogo: this.props.company.logo,
        errorText: null
      });
    }
  }

  handleCompanyNameChange(event) {
    this.setState({ companyName: event.target.value });
  }

  handleCompanyLogoChange(event) {
    this.setState({ companyLogo: event.target.value });
  }

  save() {
    if (!this.validate()) {
      return;
    }

    const company = {
      id: this.props.company.id,
      name: this.state.companyName,
      logo: this.state.companyLogo
    };
    const requestUrl = 'http://localhost:8080/company';
    const method = this.props.company.id > 0 ? 'PUT' : 'POST';

    fetch(requestUrl, {
      method: method,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(company)
    })
      .then(response => {
        this.props.onCompanyChange();
        this.toggle();
      });
  }

  validate() {
    if (this.state.companyName == null || this.state.companyName.length < 1) {
      this.setState({ errorText: 'Ettevõtte nimi on puudu!' });
      return false;
    }
    if (this.state.companyLogo == null || this.state.companyLogo.length < 1) {
      this.setState({ errorText: 'Ettevõtte logo on puudu!' });
      return false;
    }

    this.setState({ errorText: null });
    return true;
  }

  render() {
    return (
      <div className="companyRowButtons">
        <Button color={this.props.color} onClick={this.toggle}>{this.props.buttonLabel}</Button>
        <Modal isOpen={this.state.modal} toggle={this.toggle}>
          <ModalHeader toggle={this.toggle}>{this.props.modalTitle}</ModalHeader>
          <ModalBody>
            <ValidationError errorText={this.state.errorText} />
            <Form>
              <FormGroup>
                <Input
                  placeholder="Ettevõtte nimi"
                  value={this.state.companyName}
                  onChange={this.handleCompanyNameChange}
                />
              </FormGroup>
              <FormGroup>
                <Input
                  placeholder="Ettevõtte logo"
                  value={this.state.companyLogo}
                  onChange={this.handleCompanyLogoChange}
                />
              </FormGroup>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.save}>Salvesta</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
