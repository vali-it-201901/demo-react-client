import React, { Component } from 'react';
import { Table } from 'reactstrap';
import Company from './Company';

export default class Companies extends Component {
  render() {
    return (
        <Table>
        <thead className="thead-dark">
          <tr>
            <th>ID</th>
            <th>Nimi</th>
            <th>Logo</th>
            <th>Tegevused</th>
          </tr>
        </thead>
        <tbody>
          {
            this.props.companies.map(company => (
              <Company
                company={company}
                onCompanyChange={this.props.onCompanyChange}
              />
            ))
          }
        </tbody>
        </Table>
    );
  }

}
