import React, { Component } from 'react';
import { Alert } from 'reactstrap';

export default class ValidationError extends Component {
  render() {
      if (this.props.errorText) {
        return (
          <Alert color="danger">
            {this.props.errorText}
          </Alert>
        );
      } else {
        return null;
      }
  }
}
