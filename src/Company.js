import React, { Component } from 'react';
import { Button } from 'reactstrap';
import CompanyModal from './CompanyModal';

export default class Company extends Component {
  constructor(props) {
    super(props);

    this.deleteCompany = this.deleteCompany.bind(this);
  }

  deleteCompany(id) {
    if (window.confirm('Oled sa kindel, et soovid antud ettevõtet kustutada?')) {
      fetch('http://localhost:8080/company/' + id, { method: 'DELETE' })
        .then(response => this.props.onCompanyChange());
    }
  }

  render() {
    return (
      <tr>
        <td>{this.props.company.id}</td>
        <td>{this.props.company.name}</td>
        <td><img src={this.props.company.logo} height="50" /></td>
        <td>
          <div className="companyRowButtons">
            <Button
              className="btn btn-danger"
              onClick={ () => this.deleteCompany(this.props.company.id) }
            >
              Kustuta
            </Button>
          </div>
          <CompanyModal
            buttonLabel="Muuda"
            modalTitle="Ettevõtte muutmine"
            company={this.props.company}
            onCompanyChange={this.props.onCompanyChange}
            color="dark"
          />
        </td>
      </tr>
    );
  }
}
